﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Core.Service
{
    public class App : IApp
    {
        private IConfiguration _config;
        private ILogger<App> _logger;

        public App(ILogger<App> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public void Run(string[] args)
        {
            string stringFromAppSettingsJson = _config.GetValue<string>("TestString");
            _logger.LogInformation("The current environment is: {@EnvironmentName}", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
            _logger.LogInformation("The message from appsettings json: {@stringFromAppSettingsJson}", stringFromAppSettingsJson);
        }
    }
}